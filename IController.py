import requests
from time import sleep

class Way:
    def __init__(self):
        self.status = None

    def open(self):
        self.status = requests.get("http://192.168.43.56/open").content

    def close(self):
        self.status = requests.get("http://192.168.43.56/close").content

    def get_status(self):
        return self.status



way = Way()
way.close()

print(way.get_status())
