Все завязано на классе Recognizer из файла recognizer.py <br>
Для запуска на своем компьютере - установить все необходимые библиотеки и вызвать ряд функций экземпляра класса:<br>
load_models() - грузит нужные модели <br>
prep_identityDB() - грузит базу лиц (папка faces_db, в будущем - из MongoDB) <br>
start_recognizer() - запускает распознавание <br>

Требования:<br>
(Зависимости подтягиваются автоматически)<br>
tensorflow<br>
Keras<br>
opencv-python<br>
numpy<br>
scipy<br>
matplotlib<br>
argparse<br>


Пример использования в main.py

P.S: На данный момент распознавание пола работает кривовато, не обижайтесь