import argparse
from recognizer import *

ap = argparse.ArgumentParser()
ap.add_argument("-l", "--liveness", type=str, required=True,
                help="path to trained model")
ap.add_argument("-g", "--gender", type=str, required=True,
                help="path to gender detection model")
ap.add_argument("-d", "--detector", type=str, required=True,
                help="path to OpenCV's deep learning face detector")
ap.add_argument("-db", "--database", type=str, required=True,
                help="path to face db")
args = vars(ap.parse_args())


recognizer = Recognizer()

recognizer.load_models(args["liveness"], args["gender"], args["detector"])

recognizer.prep_identityDB(args["database"])

recognizer.start_recognizer(0)

